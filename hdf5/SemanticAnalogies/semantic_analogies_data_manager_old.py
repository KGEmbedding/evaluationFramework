import pandas as pd
import numpy as np
import base64
import h5py

class DataManager:
    def __init__(self):
        print('SemanticAnalogies data manager initialized')

    @staticmethod
    def create_vocab(vectors):
        print('vectors:' +str(len(vectors)))
        words = vectors.keys()
        vocab_size = len(words)
        vocab = {w: idx for idx, w in enumerate(words)}

        return vocab

    @staticmethod
    def read_data(vector_filename, gold_standard_file):
        vector_file = h5py.File(vector_filename, 'r')
        vector_group = vector_file["Vectors"]
        
        vectors = {}
        data = list()
        ignored = list()
        
        with open(gold_standard_file, 'r') as f:
            for line in f:
                quadruplet = line.rstrip().split()

                try:
                    key_0 = base64.b32encode(quadruplet[0])
                    key_1 = base64.b32encode(quadruplet[1])
                    key_2 = base64.b32encode(quadruplet[2])
                    key_3 = base64.b32encode(quadruplet[3])

                    value_0 = vector_group[key_0][0]
                    value_1 = vector_group[key_1][0]
                    value_2 = vector_group[key_2][0]
                    value_3 = vector_group[key_3][0]

                    vectors[key_0] = value_0
                    vectors[key_1] = value_1
                    vectors[key_2] = value_2
                    vectors[key_3] = value_3

                    data.append([key_0, key_1, key_2, key_3])
                except KeyError:
                    ignored.append(quadruplet)
        return (vectors, data, ignored)