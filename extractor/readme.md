# Extractor

## Simple extractor

The **simpleExtractor.py** removes the angular brackets from the entity name.

Input/Output : **_txt_** file **_tab separated_** 

## Extractor

The **extractor.py** convertes a Gensim model into txt file.

Input : file that contains **_gensim model_** 

Output : **_txt_** file **_tab separated_** and with entities without angular brackets
