# Implementation of some ways to do intrinsic evaluation
# by analysing the categorisational aspect of the embedding.
# Yusuf Kocak and Emma Ahrens, August 2018

import pandas as pd
import numpy as np
from scipy import spatial
np.set_printoptions(precision=20)

# Calculate the categorization and the coherence metric from
# https://arxiv.org/abs/1803.04488 (Concept2vec: Metrics for
# Evaluating Quality of Embeddings for Ontological Concepts)
# for the vectors of the national football teams and the countries

# Get data of national football teams (uri, rank and vectors that are
# obtained by RDF2Vec)
filename = 'data/original_teams.csv'
teams = pd.read_csv(filename, converters={
    'vec': lambda x: np.fromstring(x[1:-1], sep=' ')
})

# Select only those rows that contain a not-NaN vector
def has_vec(row):
    return len(row['vec']) > 0

teams['concept'] = 't'
teams = teams.loc[teams.apply(has_vec, axis=1)]
concept_team = teams.iloc[0]
teams = teams.iloc[1:]

# Get data of countries (uri and vectors obtained by RDF2Vec)
filename = 'data/original_countries.csv'
countries = pd.read_csv(filename, converters={
    'vec': lambda x: np.fromstring(x[1:-1], sep=' ')
})

countries['concept'] = 'c'
concept_country = countries.iloc[0]
countries = countries.iloc[1:]

# Categorization metric for the vectors in teams
# (How is the cosine similarity between the average team vector
# and the concept team vector?)

average_team = np.mean(np.vstack(teams['vec'].values), axis=0)
similarity = 1 - spatial.distance.cosine(average_team, concept_team['vec'])
print("Cosine similarity between average team vector and team concept vector:")
print(similarity)

# Categorization metric for the vectors of countries
# (How is the cosine similarity between the average team vector
# and the concept team vector?)

average_country = np.mean(np.vstack(countries['vec'].values), axis=0)
similarity = 1 - spatial.distance.cosine(average_country, concept_country['vec'])
print("Cosine similarity between average country vector and country concept vector:")
print(similarity)

# Coherence metric for concept_team and concept_country
# (How many of the nearest n vectors of the concept vector are
# of the same concept)
n = 100

def compute_sim_t(row):
    return np.abs(-spatial.distance.cosine(concept_team['vec'], row['vec'])+1)

def compute_sim_c(row):
    return np.abs(-spatial.distance.cosine(concept_country['vec'], row['vec'])+1)

teams['sim_t'] = teams.apply(compute_sim_t, axis=1)
teams['sim_c'] = teams.apply(compute_sim_c, axis=1)
teams.sort_values(by='sim_t', ascending=True, inplace=True)

countries['sim_t'] = countries.apply(compute_sim_t, axis=1)
countries['sim_c'] = countries.apply(compute_sim_c, axis=1)
countries.head()

all_vecs = pd.concat([teams, countries], sort=False)

all_vecs.sort_values(by='sim_t', ascending=True, inplace=True)
in_radius_t = all_vecs.iloc[:n]
coherence_t = len(in_radius_t[in_radius_t['concept']=='t'])

all_vecs.sort_values(by='sim_c', ascending=True, inplace=True)
in_radius_c = all_vecs.iloc[:n]
coherence_c = len(in_radius_c[in_radius_c['concept']=='c'])

print('Coherence metric of topic teams: ' + coherence_t)
print('Coherence metric of topic countries: ' + coherence_c)

