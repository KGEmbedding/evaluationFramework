from sklearn import metrics
from sklearn import datasets, linear_model


import numpy as np
import pandas as pd
from collections import defaultdict
import extrinsic_properties_data_manager as data_manager
import extrinsic_properties_model as model
import csv

class Evaluator:
    def __init__(self):
        print("Extrinsic properties evaluator init")

    @staticmethod
    def evaluate(vectors, results_folder):
        scores = list()

        #Categorization property
        gold_standard_filenames = ['teams', 'countries']

        for i in range(len(gold_standard_filenames)):
            gold_standard_filename = gold_standard_filenames[i]

            gold_standard_file = 'ExtrinsicProperties/data/'+gold_standard_filename+'.txt'

            concept, data, ignored = data_manager.DataManager.read_data(vectors, gold_standard_file)
        
            print(concept)
            print(data)

            score = ''

            if data.size == 0:
                print('Extrinsic properties - categorization : Problems in merging vectors with gold standards ' + gold_standard_filename)
            else:
                if concept.size == 0 :
                    print('Extrinsic properties - categorization : Problems in computing categorization for ' + concept + ' because the vector is absent')
                else: 
                    '''
                    print('Extrinsic properties: Ignored data : ' + str(len(ignored)))

                    file_ignored = open(results_folder+'/extrinsicProperties_categorization_'+gold_standard_filename+'_ignoredData.txt',"w") 
                    for ignored_tuple in ignored.itertuples():
                        print('Extrinsic properties - categorization: Ignored data: ' + getattr(ignored_tuple,'name'))
                        file_ignored.write(getattr(ignored_tuple,'name').encode('utf-8')+'\n')
                    file_ignored.close()
                    '''
                    print('Extrinsic properties - categorization : Used data : ' + len(data)/(len(ignored)+len(data)))               
                    score = model.Model.compute_categorization_score(concept, data)
                    print('Extrinsic properties - categorization : concept -> ' + concept['name'] + ' score -> '+ score)

            scores.append({'property': 'categorization', 'concept' : concept['name'], 'num_of_entities' : len(data)/(len(ignored)+len(data)), 'score' : score})

        with open(results_folder+'/extrinsicProperties_results.csv', "wb") as csv_file:
            fieldnames = ['property', 'concept', 'num_of_entities', 'score']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            for score in scores:
                writer.writerow(score)


     
        

