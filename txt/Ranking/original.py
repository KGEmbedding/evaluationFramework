# Implementation of extrinsic evaluation by applying regression
# tasks to a given data set
# Yusuf Kocak and Emma Ahrens, August 2018

import pandas as pd
import numpy as np
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import PolynomialFeatures
import random

filename = 'data/teams.csv'
teams = pd.read_csv(filename, converters={
    'vec': lambda x: np.fromstring(x[1:-1], sep=' ')
})

# Select only those rows that contain a not-NaN vector
def has_vec(row):
    return len(row['vec']) > 0

teams = teams.loc[teams.apply(has_vec, axis=1)]
teams = teams.loc[1:]

# Save the errors to calculate mean and standard deviation
errors = []

for i in range(1,10):
    train_idx = list(sorted(random.sample(list(range(len(teams))), 170)))

    X = teams['vec']
    y = teams['rank']
    X = np.vstack(X)
    y = np.vstack(y)
    X_train = X[train_idx]
    y_train = y[train_idx]

    # Try linear, ridge and logistic regression. Then with preprocessing.
    # Linear
    model = LinearRegression()
    model.fit(X_train, y_train)

    # Predict on whole data set and write into column of teams
    pred = model.predict(X)
    teams['linear_pred'] = pred.flatten()
    teams

    # Ridge
    model = Ridge()
    model.fit(X_train, y_train)

    # Predict on whole data set and write into column of teams
    pred = model.predict(X)
    teams['ridge_pred'] = pred.flatten()
    teams

    # Logistic
    y_train = y_train.flatten()
    model = LogisticRegression()
    model.fit(X_train, y_train)

    # Predict on whole data set and write into column of teams
    pred = model.predict(X)
    teams['logistic_pred'] = pred.flatten()
    teams

    # With polynomial preprocessing
    model = PolynomialFeatures(3)
    model.fit_transform(X, y)

    X_train = X[train_idx]
    y_train = y[train_idx]

    model = LinearRegression()
    model.fit(X_train, y_train)

    # Predict on whole data set and write into column of teams
    pred = model.predict(X)
    teams['poly_prep'] = pred.flatten()
    teams

    # Calculate errors
    # Linear regression
    orig = teams.sort_values(by='rank', ascending=True, inplace=False)
    pred = teams.sort_values(by='linear_pred', ascending=True, inplace=False)
    orig = orig['rank'].tolist()
    pred = pred['rank'].tolist()
    tau_lin, _ = stats.kendalltau(orig, pred)
    rms_lin = np.sqrt(np.mean(np.power(np.asarray(orig) - np.asarray(pred), 2)))

    # Ridge
    pred = teams.sort_values(by='ridge_pred', ascending=True, inplace=False)
    pred = pred['rank'].tolist()
    tau_ridge, _ = stats.kendalltau(orig, pred)
    rms_ridge = np.sqrt(np.mean(np.power(np.asarray(orig) - np.asarray(pred), 2)))
    
    # Logistic regression
    pred = teams.sort_values(by='logistic_pred', ascending=True, inplace=False)
    pred = pred['rank'].tolist()
    tau_log, _ = stats.kendalltau(orig, pred)
    rms_log = np.sqrt(np.mean(np.power(np.asarray(orig) - np.asarray(pred), 2)))

    # Linear regression with polynomial preprocessing
    pred = teams.sort_values(by='poly_prep', ascending=True, inplace=False)
    pred = pred['rank'].tolist()
    tau_prep, _ = stats.kendalltau(orig, pred)
    rms_prep = np.sqrt(np.mean(np.power(np.asarray(orig) - np.asarray(pred), 2)))

    errors.append({
        'nr': i,
        'tau_lin': np.abs(tau_lin),
        'tau_ridge': np.abs(tau_ridge),
        'tau_log': np.abs(tau_log),
        'tau_prep': np.abs(tau_prep),
        'rms_lin': rms_lin,
        'rms_ridge': rms_ridge,
        'rms_log': rms_log,
        'rms_prep': rms_prep
    })

errors = pd.DataFrame(errors)

# Calculate mean and standard deviation
tau_lin = np.mean(errors['tau_lin'])
tau_lin_std = np.std(errors['tau_lin'])
tau_ridge = np.mean(errors['tau_ridge'])
tau_ridge_std = np.std(errors['tau_ridge'])
tau_log = np.mean(errors['tau_log'])
tau_log_std = np.std(errors['tau_log'])
tau_prep = np.mean(errors['tau_prep'])
tau_prep_std = np.std(errors['tau_prep'])

rms_lin = np.mean(errors['rms_lin'])
rms_lin_std = np.std(errors['rms_lin'])
rms_ridge = np.mean(errors['rms_ridge'])
rms_ridge_std = np.std(errors['rms_ridge'])
rms_log = np.mean(errors['rms_log'])
rms_log_std = np.std(errors['rms_log'])
rms_prep = np.mean(errors['rms_prep'])
rms_prep_std = np.std(errors['rms_prep'])

print('Tau errors:')
print('Linear regression: '+ tau_lin)
print('and its standard deviation: '+ tau_lin_std)
print('Ridge regression: '+ tau_ridge)
print('and its standard deviation: '+ tau_ridge_std)
print('Logistic regression: '+ tau_log)
print('and its standard deviation: '+ tau_log_std)
print('Linear regression with polynomial preprocessing: '+ tau_prep)
print('and its standard deviation: '+ tau_prep_std)
print('\n')

print('Root mean square errors:')
print('Linear regression: '+ rms_lin)
print('and its standard deviation: '+ rms_lin_std)
print('Ridge regression: '+ rms_ridge)
print('and its standard deviation: '+ rms_ridge_std)
print('Logistic regression: '+ rms_log)
print('and its standard deviation: '+ rms_log_std)
print('Linear regression with polynomial preprocessing: '+ rms_prep)
print('and its standard deviation: '+ rms_prep_std)
print('\n')
